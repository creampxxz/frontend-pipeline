FROM node:lts-alpine as dependencies
WORKDIR /my-frontend
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

FROM node:lts-alpine as builder
WORKDIR /my-frontend
COPY . .
COPY --from=dependencies /my-frontend/node_modules ./node_modules
RUN yarn build

FROM node:lts-alpine as runner
WORKDIR /my-frontend
ENV NODE_ENV production
# If you are using a custom next.config.js file, uncomment this line.
# COPY --from=builder /my-frontend/next.config.js ./
COPY --from=builder /my-frontend/public ./public
COPY --from=builder /my-frontend/.next ./.next
COPY --from=builder /my-frontend/node_modules ./node_modules
COPY --from=builder /my-frontend/package.json ./package.json

EXPOSE 3000
CMD ["yarn", "start"]